import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Vibration } from '@ionic-native/vibration/ngx';

import { IonicModule } from '@ionic/angular';

import { GameCatchphrasePageRoutingModule } from './game-catchphrase-routing.module';

import { GameCatchphrasePage } from './game-catchphrase.page';
import { FlippingCardComponent } from './flipping-card/flipping-card.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GameCatchphrasePageRoutingModule,
    
  ],
  declarations: [GameCatchphrasePage, FlippingCardComponent],

  providers: [Vibration]
})
export class GameCatchphrasePageModule {}
