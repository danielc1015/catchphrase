import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GameCatchphrasePage } from './game-catchphrase.page';

const routes: Routes = [
  {
    path: '',
    component: GameCatchphrasePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GameCatchphrasePageRoutingModule {}
