import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-flipping-card',
  templateUrl: './flipping-card.component.html',
  styleUrls: ['./flipping-card.component.scss'],
})
export class FlippingCardComponent implements OnInit {

  flipped: boolean = false;

  constructor() {

  }
  ngOnInit() {
  }

  flip(){
    this.flipped = !this.flipped;
  }


}
