import { Component, OnInit, ViewChild } from '@angular/core';
import { StateService } from '../services/state.service';
import { FlippingCardComponent } from './flipping-card/flipping-card.component';
import { Vibration } from '@ionic-native/vibration/ngx';

@Component({
  selector: 'app-game-catchphrase',
  templateUrl: './game-catchphrase.page.html',
  styleUrls: ['./game-catchphrase.page.scss'],
})
export class GameCatchphrasePage implements OnInit {

  playing: boolean;
  totalSeconds: number;
  secondsLeft: number;
  secondsToStart;
  beep;
  buzzer;
  secondsLeftSound;
  phrases: any[];
  countDownTimer;
  beepTimer;
  switching;
  frontPhrase;
  backPhrase;
  counter;
  @ViewChild('card') card: FlippingCardComponent;

  constructor(
    private _state: StateService,
    private vibration: Vibration
    ) { }

  ngOnInit() {
    this.beep = new Audio('../assets/audio/beeps.mp3');
    this.buzzer = new Audio('../assets/audio/buzzer.mp3');
    this.secondsLeftSound = new Audio('../assets/audio/3-seconds-countdown.mp3');
    this.beep.load();
    this.buzzer.load();
    this.secondsLeftSound.load();
    this.playing = false;
    this.switching = false
  }

  play() {
    this.vibration.vibrate([500, 500, 500, 500, 500, 500]);
    this.counter = 0;
    this.secondsLeftSound.play();
    this.setTotalSeconds();
    this.secondsToStart = 3;
    setTimeout(() => {
      this.secondsToStart = 2;
    }, 1000);
    setTimeout(() => {
      this.secondsToStart = 1;
    }, 2000);
    setTimeout(() => {
      this.secondsToStart = null;
    }, 3000);

    setTimeout(() => {
      this.vibration.vibrate(1000)
      this.playing = true;
      this.startCountDown();
      this.startBeeperInterval();
      this.frontPhrase = this.pickPhrase();
    }, 3000);
    
  }

  startCountDown() {
    this.secondsLeft = this.totalSeconds;
    this.countDownTimer = setInterval(()=> {
      if (this.secondsLeft > 0) {
        console.log(this.secondsLeft);
        this.secondsLeft -= 125;
      } else {
        this.stop();
      }
    }, 125);
    // this.countDown = setInterval(()=> {
    //   this.beep.play();
    // }, 1500);
  }

  startBeeperInterval() {
    this.beepTimer = setInterval(() => {
      if (this.secondsLeft >= 45000 && (this.secondsLeft % 1000 == 0)) {
        this.playBeep(1000);
      }
      
      if (this.secondsLeft >= 35000 && this.secondsLeft < 45000 && (this.secondsLeft % 750 == 0)) {
        this.playBeep(750);
      }
      
      if (this.secondsLeft >= 25000 && this.secondsLeft < 35000 && (this.secondsLeft % 500 == 0)) {
        this.playBeep(500);
      }
      
      if (this.secondsLeft >= 15000 && this.secondsLeft < 25000 && (this.secondsLeft % 250 == 0)) {
        this.playBeep(125);
      }

      if (this.secondsLeft >= 0 && this.secondsLeft < 15000 && (this.secondsLeft % 125 == 0)) {
        this.playBeep(125);
      }
    }, 125);
  }

  async playBeep(interval) {
    await setTimeout(() => {
      this.beep.play();
    }, interval);
  }

  stop() {
    this.vibration.vibrate([100, 100, 100, 100, 100, 100, 100]);
    clearInterval(this.countDownTimer);
    clearInterval(this.beepTimer);
    this.buzzer.play();
    this.playing = false;
    this.secondsLeft = 0;
    this.totalSeconds = 0;
  }

  setTotalSeconds() {
    const min = 40; // 65
    const max = 42;  // 95
    this.totalSeconds = Math.floor(Math.random() * (max - min) + min) *1000;
    console.log(this.totalSeconds);
  }

  nextPhrase() {
    this.counter++;
    this.card.flip();
    this.vibration.vibrate(500);
    if (this.counter % 2 == 0) {
      this.frontPhrase = this.pickPhrase();
    } else {
      this.backPhrase = this.pickPhrase();
    }
  }

  pickPhrase() {
    const position = Math.floor(Math.random() * this._state.catchphrasePhrases.length);
    const phrase = this._state.catchphrasePhrases[position];
    this._state.catchphrasePhrases.splice(position, 1);
    return phrase;
  }

}
