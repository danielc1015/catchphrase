import { Injectable } from '@angular/core';
import { phrases } from '../../assets/collections/catchphrase-phrases';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  catchphrasePhrases: any[];

  constructor() {
    this.catchphrasePhrases = phrases;
  }



}
